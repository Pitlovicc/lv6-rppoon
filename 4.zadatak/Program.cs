﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount bankAccount = new BankAccount("Filip", "Slavonski Brod", 20000);
            Memento memento = bankAccount.StoreState();
            bankAccount.UpdateBalance(13000);
            Memento memento2 = bankAccount.StoreState();
            Console.WriteLine(bankAccount.ToString());
            Console.WriteLine(memento.ToString());
            Console.WriteLine(memento2.ToString());
        }
    }
}
