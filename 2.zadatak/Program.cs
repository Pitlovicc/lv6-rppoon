﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            Box box = new Box();

            Product product1 = new Product("cipele", 2000);
            Product product2 = new Product("naocale", 140);
            box.AddProduct(product1);
            box.AddProduct(product2);

            

            Iterator iterator = (Iterator)box.GetIterator();

            for (int i = 0; i < box.Count; iterator.Next())
            {
                Console.WriteLine(iterator.Current.ToString());
            }
        }
    }
}
