﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, @"E:\2. godina ferit\4. semestar\RPOON\LV6logFile.txt");
            AbstractLogger logger2 = new ConsoleLogger(MessageType.ERROR);
            AbstractLogger logger3 = new ConsoleLogger(MessageType.INFO);
           
            logger.SetNextLogger(logger2);
            logger2.SetNextLogger(logger3);

            Console.WriteLine(logger);
            Console.WriteLine(logger2);
            Console.WriteLine(logger3);

        }
    }
}
