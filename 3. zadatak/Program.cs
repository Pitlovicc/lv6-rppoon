﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            CareTaker care = new CareTaker();
            ToDoItem todo = new ToDoItem("posao", "usisavanje", DateTime.Now);
            Memento memento = todo.StoreState();
            todo.ChangeTask("pranje");
            Memento memento2 = todo.StoreState();
            care.Adding(memento);
            care.Adding(memento2);

            Console.WriteLine(todo.ToString());
            Console.WriteLine(memento.ToString());
            Console.WriteLine(memento2.ToString());
            



        }
    }
}
