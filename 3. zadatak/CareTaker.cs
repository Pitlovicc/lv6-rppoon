﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.zadatak
{
    class CareTaker
    {
        private List<Memento> PrevoiusState;
        public CareTaker()
        {
            this.PreviousState = new List<Memento>();
        }

        public CareTaker(List<Memento> PreviousState)
        {
            this.PreviousState = PreviousState;
        }

        public Memento PreviousState { get; set; }

        public void Adding(Memento memento)
        {
            PrevoiusState.Add(memento);
        }
        public void Removing(Memento memento)
        {
            PreviousState.Remove(memento);
        }

        public Memento getting(int index)
        {
            return PrevoiusState[index];
        }
    }
}
