﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            string password = "Kalkulator123";
            StringLengthChecker stringLength = new StringLengthChecker();
            StringDigitChecker stringDigit = new StringDigitChecker();
            StringLowerCaseChecker stringLower = new StringLowerCaseChecker();
            StringUpperCaseChecker stringUpper = new StringUpperCaseChecker();

            stringLength.SetNext(stringDigit);
            stringDigit.SetNext(stringLower);
            stringLower.SetNext(stringUpper);

            Console.WriteLine(stringLength.Check(password));
            Console.WriteLine(stringDigit.Check(password));
            Console.WriteLine(stringLower.Check(password));
            Console.WriteLine(stringUpper.Check(password));



        }
    }
}
