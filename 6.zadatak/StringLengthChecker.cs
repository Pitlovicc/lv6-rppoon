﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.zadatak
{
    class StringLengthChecker : StringChecker
    {
        int length = 6;
        protected override bool PerformCheck(string stringToCheck)
        {
            if (stringToCheck.Length > length-1 )
                return true;
            else
            {
                return false;
            }
        }
    }
}
