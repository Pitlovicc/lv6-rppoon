﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            
            Note note1 = new Note("Matematika", "srijeda");
            Note note2 = new Note("Engleski", "petak");
            Note note3 = new Note("Programiranje", "utorak");

            notebook.AddNote(note1);
            notebook.AddNote(note2);
            notebook.AddNote(note3);

            Iterator iterator = (Iterator)notebook.GetIterator();

            for(int i = 0;i<notebook.Count;iterator.Next())
            {
                iterator.Current.Show();
            }
        }
    }
}
